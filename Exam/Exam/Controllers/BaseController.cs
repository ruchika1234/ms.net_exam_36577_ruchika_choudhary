﻿using Exam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace Exam.Controllers
{
    
    public class BaseController : Controller
    {
        protected ExamDBEntities dbObj { get; set; }
        public BaseController()
        {
            this.dbObj = new ExamDBEntities();
        }
    }
}