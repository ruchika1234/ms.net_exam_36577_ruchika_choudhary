﻿using Exam.Controllers;
using Exam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Exam.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Signin(EmployeeDetail employee, string ReturnUrl)
        {
            //return View();
            var MatchCount = (from emp in dbObj.EmployeeDetails.ToList()
                              where emp.username.ToLower() == employee.username.ToLower()
                              && emp.password == employee.password
                              select emp).ToList().Count();

            if (MatchCount == 1)
            {
               
                FormsAuthentication.SetAuthCookie(employee.username, false);

                //Give instruction to shoot a call for "/Home/Index" or any page asked earlier in Querystring;
                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "User Name or Password is incorrect";
                return View();
            }


        }

        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}