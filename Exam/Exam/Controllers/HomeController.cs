﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Exam.Models;
using System.Configuration;
using System.Data.SqlClient;
using Exam.Controllers;
using System.Web.Security;

namespace Exam.Controllers
{
    //ExamDBEntities exam = new ExamDBEntities();
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(EmployeeDetail employeeDetail)
        {
            if (ModelState.IsValid)
            {
                dbObj.EmployeeDetails.Add(employeeDetail);
                dbObj.SaveChanges();

                return Redirect("/Home/Index");

            }
            else
            {
                return View(employeeDetail);
            }

           
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Message = "Please Change Profile here";
            ViewBag.UserName = User.Identity.Name;

            EmployeeDetail empDetailsEdited = (from emp in dbObj.EmployeeDetails.ToList()
                                 where emp.Emp_id == id
                                 select emp).First();

            return View(empDetailsEdited);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeDetail empUpdated)
        {

            EmployeeDetail empDetailsEdited = (from emp in dbObj.EmployeeDetails.ToList()
                                 where emp.Emp_id == empUpdated.Emp_id
                                 select emp).First();

            empDetailsEdited.EmpName = empUpdated.EmpName;
            empDetailsEdited.Address = empUpdated.Address;

            dbObj.SaveChanges();

            return Redirect("/Home/Index");

        }

    }
}