﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Exam.Helper
{
    public class Validator : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool isItValid = false;
            string val = Convert.ToString(value);
            int password = Convert.ToInt32(value);
            var email = Convert.ToString(value);
            if (!Regex.Match(val, "^[A-Z][a-zA-Z]*$").Success && val.Length < 10)
            {
              
                isItValid = false;
            }
            else
            {
                isItValid = true;
            }
            return isItValid;

        }


    }

}